//
//  Bike.swift
//  MyTasks
//
//  Created by mac-001 on 6/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Bike: BaseVehicle {
  var chain: Chain
  
  override var baseMaxSpeed: Double {
    return 35
  }
  
  override var acceleration: Double {
    let acceleration = tire.accelerateCoof + brakes.accelerateCoof + accelerate.accelerateCoof + chain.accelerateCoof
    return acceleration
  }
  
  override var maxSpeed: Double {
    let maxSpeed =  baseMaxSpeed * tire.maxSpeedCoof * brakes.maxSpeedCoof * chain.maxSpeedCoof
    return maxSpeed
  }
  
  override public  var description: String {
    return super.description + ", Chain manufacture = \(chain)"
  }
  
  init(mainColor: UIColor, tire: Tire, brakes: Brakes, accelerate: Accelerate, chain: Chain) {
    self.chain = chain
    super.init(mainColor: mainColor, tire: tire, brakes: brakes, accelerate: accelerate)
    self.tireCount = 2
  }
  override init(manufacture: Manufacture, mainColor: UIColor) {
    self.chain = Chain(manufacture: manufacture)
    super.init(manufacture: manufacture, mainColor: mainColor)
    self.tireCount = 2
  }
}

