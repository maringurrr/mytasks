//
//  Task1MainViewController.swift
//  MyTasks
//
//  Created by mac-001 on 6/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Task1MainViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let aaacar = Car(manufacture: .Lamborghini, mainColor: .blue)
    let motik = Motorcicle(manufacture: .Ferrari, mainColor: .blue, siderCar: true)
    let buke1 = Bike(manufacture: .Mersedes, mainColor: .orange)

    let track1 = Track(allSegmentsIsEqual: 5, weather: .ice, temperature: .hot, longSegment: .long)
    let track2 = Track(custom2Segments: .init(weather: .rain, temperature: .cold, long: .veryShot), segment2: .init(weather: .wind, temperature: .cold, long: .long))
    
    print(aaacar)

    print(buke1)
    print(motik)
    
    print(aaacar.race(track: track1))
    print(aaacar.race(track: track2))

    print(motik.race(track: track1))
    print(motik.race(track: track2))
  }
}
