//
//  Vihicle.swift
//  MyTasks
//
//  Created by mac-001 on 6/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Vehicle: BaseVehicle {
  var engine: Engine
  
  override var baseMaxSpeed: Double {
    return 120
  }
  
  override var acceleration: Double {
    let acceleration = engine.accelerateCoof + tire.accelerateCoof + brakes.accelerateCoof + accelerate.accelerateCoof
    return acceleration
  }
  
  override var maxSpeed: Double {
    let maxSpeed =  baseMaxSpeed * engine.maxSpeedCoof * tire.maxSpeedCoof * brakes.maxSpeedCoof
    return maxSpeed
  }
  
  override public  var description: String {
    return super.description + "Engine manufacture = \(engine),"
  }
  
  init(mainColor: UIColor, engine: Engine, tire: Tire, brakes: Brakes, accelerate: Accelerate) {
    self.engine = engine
    super.init(mainColor: mainColor, tire: tire, brakes: brakes, accelerate: accelerate)
  }
  
  override init(manufacture: Manufacture, mainColor: UIColor) {
    self.engine = Engine(manufacture: manufacture)
    super.init(manufacture: manufacture, mainColor: mainColor)
  }
}
