//
//  Car.swift
//  MyTasks
//
//  Created by mac-001 on 6/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Car: Vehicle {
  
  override var baseMaxSpeed: Double {
    return 180
  }
}
