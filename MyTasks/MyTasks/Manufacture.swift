//
//  Manufacture.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum Manufacture {
  case Unknown
  case Lamborghini
  case Bugatti
  case Ferrari
  case Mclaren
  case BMV
  case Mersedes
}
