//
//  LongSegment.swift
//  MyTasks
//
//  Created by mac-001 on 6/13/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum LongSegment: Int {
  
  case veryShot = 500
  case shot = 1000
  case normal = 2000
  case long = 20000
}
