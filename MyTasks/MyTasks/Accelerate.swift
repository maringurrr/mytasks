//
//  Accelerate.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Accelerate: Property {
  
  override init() {
    super.init()
  }
  
  override init(manufacture: Manufacture) {
    super.init()
    self.manufacture = manufacture
    switch manufacture {
    case .Bugatti:
      accelerateAmplifier = .fast
    case .Lamborghini:
      accelerateAmplifier = .hiperTurbo
    case .BMV:
      accelerateAmplifier = .turbo
    case .Mclaren:
      accelerateAmplifier = .hiperTurbo
    case .Mersedes:
      accelerateAmplifier = .fast
    case .Ferrari:
      accelerateAmplifier = .turbo
    case .Unknown:
      accelerateAmplifier = .baseComplection
    }
  }
}
