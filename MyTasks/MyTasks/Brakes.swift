//
//  Brakes.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Brakes: EnvironmentProperty {
  
  override var maxSpeedCoof: Double {
    var coefficient = maxSpeedAmplifier.rawValue
    switch weatherFactor {
    case .ice:
      coefficient *= 0.7
    case .rain:
      coefficient *= 0.75
    case .wind:
      coefficient *= 1.0
    case .normal:
      coefficient *= 1.0
    }
    switch temperatureFactor {
    case .cold:
      coefficient *= 0.9
    case .hot:
      coefficient *= 1.1
    case .normal:
      coefficient *= 1.0
    }
    return coefficient
  }
  
  override var accelerateCoof: Double {
    
    var coefficient = accelerateAmplifier.rawValue
    switch weatherFactor {
    case .ice:
      coefficient *= 0.9
    case .rain:
      coefficient *= 0.8
    case .wind:
      coefficient *= 0.9
    case .normal:
      coefficient *= 1.0
    }
    switch temperatureFactor {
    case .cold:
      coefficient *= 0.8
    case .hot:
      coefficient *= 1.1
    case .normal:
      coefficient *= 1.0
    }
    return coefficient
  }
  
  override init() {
    super.init()
  }
  
  override init(manufacture: Manufacture) {
    super.init()
    
    self.manufacture = manufacture
    switch manufacture {
    case .Bugatti:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .baseComplection
    case .Lamborghini:
      maxSpeedAmplifier = .fast
      accelerateAmplifier = .fast
    case .BMV:
      maxSpeedAmplifier = .baseComplection
      accelerateAmplifier = .hiperTurbo
    case .Mclaren:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .fast
    case .Mersedes:
      maxSpeedAmplifier = .slow
      accelerateAmplifier = .hiperTurbo
    case .Ferrari:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .turbo
    case .Unknown:
      maxSpeedAmplifier = .baseComplection
      accelerateAmplifier = .fast
    }
  }
  
  
}

