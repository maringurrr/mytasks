//
//  TemperatureFactor.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum TemperatureFactor {
  case cold
  case normal
  case hot
}
