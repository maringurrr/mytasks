//
//  Task3ViewController.swift
//  MyTasks
//
//  Created by mac-001 on 6/30/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Task3ViewController: UIViewController {

  @IBOutlet weak var button1: UIButton!
  
  @IBOutlet weak var button2: UIButton!
  @IBOutlet weak var button3: UIButton!
  @IBOutlet weak var button4: UIButton!
  @IBOutlet weak var button5: UIButton!
  @IBOutlet weak var button6: UIButton!
  @IBOutlet weak var button7: UIButton!
  @IBOutlet weak var button8: UIButton!
  @IBOutlet weak var button9: UIButton!
  @IBOutlet weak var button0: UIButton!
  @IBOutlet weak var buttonX: UIButton!
  @IBOutlet weak var buttonY: UIButton!
  @IBOutlet weak var buttonGreen: UIButton!
  
//  override func viewDidLayoutSubviews() {
//
//    configureButton(button: button1)
//    configureButton(button: button2)
//    configureButton(button: button3)
//    configureButton(button: button4)
//    configureButton(button: button5)
//    configureButton(button: button6)
//    configureButton(button: button7)
//    configureButton(button: button8)
//    configureButton(button: button9)
//    configureButton(button: button0)
//    configureButton(button: buttonX)
//    configureButton(button: buttonY)
//    configureButton(button: buttonGreen)
//  }
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
