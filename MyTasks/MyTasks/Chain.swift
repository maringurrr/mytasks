//
//  Chain.swift
//  MyTasks
//
//  Created by mac-001 on 6/14/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Chain: Property {
  
  override init() {
    super.init()
  }
  
  override init(manufacture: Manufacture) {
    super.init()
    self.manufacture = manufacture
    switch manufacture {
    case .Bugatti:
      accelerateAmplifier = .fast
      maxSpeedAmplifier = .hiperTurbo
    case .Lamborghini:
      accelerateAmplifier = .hiperTurbo
      maxSpeedAmplifier = .fast
    case .BMV:
      accelerateAmplifier = .turbo
      maxSpeedAmplifier = .baseComplection
    case .Mclaren:
      accelerateAmplifier = .hiperTurbo
      maxSpeedAmplifier = .slow
    case .Mersedes:
      accelerateAmplifier = .fast
      maxSpeedAmplifier = .turbo
    case .Ferrari:
      accelerateAmplifier = .turbo
      maxSpeedAmplifier = .turbo
    case .Unknown:
      accelerateAmplifier = .baseComplection
      maxSpeedAmplifier = .baseComplection
    }
  }
}
