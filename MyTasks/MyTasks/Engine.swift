//
//  Engine.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Engine: Property {

  override init() {
    super.init()
  }
  
  override init(manufacture: Manufacture) {
    super.init()
    self.manufacture = manufacture
    switch manufacture {
    case .Bugatti:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .hiperTurbo
    case .Lamborghini:
      maxSpeedAmplifier = .turbo
      accelerateAmplifier = .fast
    case .BMV:
      maxSpeedAmplifier = .slow
      accelerateAmplifier = .hiperTurbo
    case .Mclaren:
      maxSpeedAmplifier = .fast
      accelerateAmplifier = .turbo
    case .Mersedes:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .baseComplection
    case .Ferrari:
      maxSpeedAmplifier = .turbo
      accelerateAmplifier = .turbo
    case .Unknown:
      maxSpeedAmplifier = .turbo
      accelerateAmplifier = .slow
    }
  }
}
