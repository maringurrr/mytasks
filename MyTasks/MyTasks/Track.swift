//
//  Track.swift
//  MyTasks
//
//  Created by mac-001 on 6/13/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Track {
  
  var segments = [Segment] ()
  
  init(custom2Segments segment1: Segment, segment2: Segment) {
    segments.append(segment1)
    segments.append(segment2)
  }
  
  init(custom4Segments segment1: Segment, segment2: Segment, segment3: Segment, segment4: Segment) {
    segments.append(segment1)
    segments.append(segment2)
    segments.append(segment3)
    segments.append(segment4)
  }
  
  init(custom8Segments segment1: Segment, segment2: Segment, segment3: Segment, segment4: Segment, segment5: Segment, segment6: Segment, segment7: Segment, segment8: Segment) {
    segments.append(segment1)
    segments.append(segment2)
    segments.append(segment3)
    segments.append(segment4)
    segments.append(segment5)
    segments.append(segment6)
    segments.append(segment7)
    segments.append(segment8)
  }
  
  init(allSegmentsIsEqual count: Int, weather: WeatherFactor,  temperature: TemperatureFactor, longSegment: LongSegment) {
    for _ in 1...count {
      let segment: Segment = .init(weather: weather, temperature: temperature, long: longSegment)
      self.segments.append(segment)
    }
  }
}
