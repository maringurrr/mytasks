//
//  Task2TableViewCell.swift
//  MyTasks
//
//  Created by mac-001 on 6/29/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class Task2TableViewCell: UITableViewCell {

  @IBOutlet weak var idLabel: UILabel!
  @IBOutlet weak var userIdLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
}
