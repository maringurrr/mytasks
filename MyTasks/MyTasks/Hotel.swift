//
//  Hotel.swift
//  MyTasks
//
//  Created by mac-001 on 7/10/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Hotel {
  var name: String
  var photo: [UIImage]
  var reviews: [String]
  var servise: [UIImage]
  var graf: [UIImage]
  
  init() {
    self.name = "NewYorkCity"
    photo = []
    reviews = []
    servise = []
    graf = []
  }
  
  init(name: String) {
    self.name = name
    photo = []
    reviews = []
    servise = []
    graf = []
  }
}
