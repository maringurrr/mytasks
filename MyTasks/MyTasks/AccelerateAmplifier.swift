//
//  AccelerateAmplifier.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum AccelerateAmplifier: Double {
  case baseComplection = 10
  case turbo = 14
  case slow = 8
  case fast = 12
  case hiperTurbo = 20
}
