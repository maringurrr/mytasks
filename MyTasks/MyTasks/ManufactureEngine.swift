//
//  ManufactureEngine.swift
//  MyTasks
//
//  Created by mac-001 on 6/13/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum ManufactureEngine {
  case Unknown
  case Lamborghini
  case Nissan
  case Bugatti
  case Hennessey
  case GTurbo
}
