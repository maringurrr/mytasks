//
//  Button.swift
//  MyTasks
//
//  Created by mac-001 on 7/4/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

@IBDesignable
class MyButton: UIButton {

  @IBInspectable var cornerRadius: CGFloat {
    get {
      return layer.cornerRadius
    }
    set {
      layer.cornerRadius = newValue
      layer.masksToBounds = newValue > 0
    }
  }
  
  @IBInspectable var borderWidth: CGFloat {
    get {
      return layer.borderWidth
    }
    set {
      layer.borderWidth = newValue
    }
  }
  
  @IBInspectable var borderColor: UIColor? {
    get {
      return UIColor(cgColor: layer.borderColor!)
    }
    set {
      layer.borderColor = newValue?.cgColor
    }
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    
    self.layer.cornerRadius = self.bounds.width / 2
  }
}
