//
//  MyTableViewController.swift
//  MyTasks
//
//  Created by mac-001 on 7/11/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class MyTableViewController: UITableViewController {
  
  var hotel = Hotel()
  
  @IBOutlet var mainTableView: UITableView!
  
  @IBOutlet weak var rewiewCollectionView: UICollectionView!
  
  @IBOutlet weak var collectionHighConstraint: NSLayoutConstraint!
  
  @IBOutlet weak var serviseCollectionView: UICollectionView!
  @IBOutlet weak var grafCollectionView: UICollectionView!
  @IBOutlet weak var photoCollectionView: UICollectionView!
  
  var helper = true
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    if helper {
      collectionHighConstraint.constant = rewiewCollectionView.contentSize.height
      mainTableView.reloadData()
      helper = false
    }
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    guard !helper else {return 120}
    
    if indexPath.section == 0 {
      if indexPath.row == 0 {
        return 160
      } else {
        return 120
      }
    }
    if indexPath.section == 1 {
      return rewiewCollectionView.contentSize.height
    }
    return 120
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    rewiewCollectionView.register(UINib.init(nibName: "RewardsCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CellRewards")
    if let flowLayout = rewiewCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
    }
    
    hotel.photo.append(UIImage(named: "1")!)
    hotel.photo.append(UIImage(named: "2")!)
    hotel.photo.append(UIImage(named: "3")!)
    hotel.photo.append(UIImage(named: "4")!)
    hotel.photo.append(UIImage(named: "5")!)
    hotel.photo.append(UIImage(named: "6")!)
    hotel.photo.append(UIImage(named: "7")!)
    hotel.photo.append(UIImage(named: "8")!)
    hotel.photo.append(UIImage(named: "9")!)
    
    hotel.reviews.append("We spent one night at the Grand Hotel and Spa. We were thoroughly impressed with the manner in which we were looked after from the moment we arrived. Each member of staff we met was so polite & courteous. We were upgraded to a deluxe suite as we had been waiting less than 10 minutes at reception to check in. We were so appreciative of this gesture by the hotel as the small wait really hadn't inconvenienced us in any way. The room itself was beautifully decorated, with a very comfortable bed & lovely bathrooms. The hotel is ideally situated for exploring York. We would certainly recommend this hotel and hope to visit again on future trips to York.")
    
    hotel.reviews.append("Wonderful hotel, the staff were helpful and friendly, my friend and I had a lovely weekend at this luxurious hotel! I will stay here again.")
    hotel.reviews.append("The gym is very small and ill equipped for a hotel of this kind.")
    hotel.reviews.append("We loved everything about our stay! From the warm welcome when we arrived to the spacious room, amenities and the turndown service. We will definitely book again if visit York and will be on the lookout from their other hotels when we travel.")
    hotel.reviews.append("absolutely fabulous! Great quality hotel and facilities. the staff are ver friendly and nothing is too much trouble. Excellent standards. The car parking is expensive.")
    
    hotel.graf.append(UIImage(named: "114")!)
    
    hotel.servise.append(UIImage(named: "111")!)
    hotel.servise.append(UIImage(named: "112")!)
    hotel.servise.append(UIImage(named: "113")!)
  }
}

// MARK: UICollectionViewDataSource

extension MyTableViewController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    
    var count: Int?
    
    switch collectionView {
    case self.serviseCollectionView:
      count = hotel.servise.count
    case self.grafCollectionView:
      count = hotel.graf.count
    case self.photoCollectionView:
      count = hotel.photo.count
    case self.rewiewCollectionView:
      count = hotel.reviews.count
    default:
      count = 0
    }
    return count!
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    
    let cell: UICollectionViewCell?
    
    switch collectionView {
    case self.serviseCollectionView:
      let cellServise = collectionView.dequeueReusableCell(withReuseIdentifier: "CellServise", for: indexPath) as! ServiseCollectionViewCell
      cellServise.serviseImage.image = hotel.servise[indexPath.row]
      return cellServise
      
    case self.grafCollectionView:
      let cellGraf = collectionView.dequeueReusableCell(withReuseIdentifier: "CellGraf", for: indexPath) as! GrafCollectionViewCell
      cellGraf.imageGraf.image = hotel.graf[indexPath.row]
      return cellGraf
      
    case self.photoCollectionView:
      
      let cellPhoto = collectionView.dequeueReusableCell(withReuseIdentifier: "CellPhoto", for: indexPath) as! PhotoCollectionViewCell
      cellPhoto.photoImage.image = hotel.photo[indexPath.row]
      return cellPhoto
      
    case self.rewiewCollectionView:
      let cellRewiew = collectionView.dequeueReusableCell(withReuseIdentifier: "CellRewards", for: indexPath) as! RewardsCollectionViewCell
      cellRewiew.labelRewards.text = hotel.reviews[indexPath.row]
      return cellRewiew
      
    default:
      cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuseIdentifier", for: indexPath)
      return cell!
    }
  }
}

