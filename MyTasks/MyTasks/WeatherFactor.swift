//
//  WeatherFactor.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum WeatherFactor: Double{
  case ice
  case rain
  case normal
  case wind
}
