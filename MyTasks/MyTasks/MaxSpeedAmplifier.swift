//
//  MaxSpeedAmplifier.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

enum MaxSpeedAmplifier: Double {
  case baseComplection = 1.0
  case turbo = 1.2
  case slow = 0.9
  case hiperTurbo = 1.4
  case fast = 1.1
}
