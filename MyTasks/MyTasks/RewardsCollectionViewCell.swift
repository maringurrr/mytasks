//
//  RewardsCollectionViewCell.swift
//  MyTasks
//
//  Created by mac-001 on 7/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class RewardsCollectionViewCell: UICollectionViewCell {

  @IBOutlet weak var widthConstraint: NSLayoutConstraint!
  @IBOutlet weak var labelRewards: UILabel!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
    self.contentView.translatesAutoresizingMaskIntoConstraints = false
    let screenWidth = UIScreen.main.bounds.size.width
    widthConstraint.constant = screenWidth - (2 * 20)
  }
}
