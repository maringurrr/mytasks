//
//  Task2MainViewController.swift
//  MyTasks
//
//  Created by mac-001 on 6/27/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class Task2MainViewController: UIViewController {
  
  @IBOutlet weak var tableView: UITableView!
  
  let queueForSaveArray = DispatchQueue(label: "com.maringur.mytask")
  var arrayForPosts = [Message]()
  
  @IBAction func itemButton(_ sender: UIBarButtonItem) {
    DispatchQueue.global().async {
      for _ in 1...5 {
        self.getPostsForUser()
        sleep(1)
      }
    }
  }
  
  @IBAction func getButton(_ sender: UIBarButtonItem) {
    
    DispatchQueue.global().async {
      for _ in 1...5 {
        self.getPostsForUser()
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func getPostsForUser() {
    
    let id = Int(arc4random_uniform(10) + 1)
    print(id)
    Alamofire.request("https://jsonplaceholder.typicode.com/posts").responseArray { (response: DataResponse<[Message]>) in
      
      DispatchQueue.main.async {
        let personArrayResponse = response.result.value
        if let personArrayResponse = personArrayResponse {
          let userArrayPosts = personArrayResponse.filter{i in i.id == id}
          
          for personResponse in userArrayPosts {
            for (index, element) in self.arrayForPosts.enumerated() {
              
              if element.id == personResponse.id {
                self.arrayForPosts.remove(at: index)
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .left)
                self.tableView.endUpdates()
              }
            }
            self.arrayForPosts.insert(personResponse, at: 0)
            self.arrayForPosts = self.arrayForPosts.sorted(by: { $0.id! < $1.id! })
            var number = 0
            for (index, element) in self.arrayForPosts.enumerated() {
              if element.id == personResponse.id {
                number = index
              }
            }
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [IndexPath(row: number, section: 0)], with: .left)
            self.tableView.endUpdates()
          }
        }
      }
    }
  }
}

extension Task2MainViewController: UITableViewDelegate, UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return arrayForPosts.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath) as! Task2TableViewCell
    cell.idLabel.text = "\(self.arrayForPosts[indexPath.row].id ?? 0)"
    cell.userIdLabel.text = "\(self.arrayForPosts[indexPath.row].userId ?? 0)"
    cell.titleLabel.text = "\(self.arrayForPosts[indexPath.row].title ?? "")"
    return cell
  }
}
