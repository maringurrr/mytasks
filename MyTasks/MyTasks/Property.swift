//
//  Property.swift
//  MyTasks
//
//  Created by mac-001 on 6/15/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Property: CustomStringConvertible {
  
  public var description: String {
    return "\(manufacture)"
  }
  
  var maxSpeedCoof: Double {
    return maxSpeedAmplifier.rawValue
  }
  var accelerateCoof: Double {
    return accelerateAmplifier.rawValue
  }
  var manufacture: Manufacture
  var maxSpeedAmplifier: MaxSpeedAmplifier
  var accelerateAmplifier: AccelerateAmplifier
  
  init() {
    self.manufacture = .Unknown
    self.maxSpeedAmplifier = .baseComplection
    self.accelerateAmplifier = .baseComplection
  }
  
  init(manufacture: Manufacture) {
    self.manufacture = manufacture
    self.maxSpeedAmplifier = .baseComplection
    self.accelerateAmplifier = .baseComplection
  }
}
