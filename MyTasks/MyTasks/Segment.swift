//
//  Segment.swift
//  MyTasks
//
//  Created by mac-001 on 6/13/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Segment {
  var weather: WeatherFactor
  var temperature: TemperatureFactor
  var long: LongSegment
  
  init(weather: WeatherFactor, temperature: TemperatureFactor, long: LongSegment) {
    self.weather = weather
    self.temperature = temperature
    self.long = long
  }
  
  init() {
    self.weather = .normal
    self.temperature = .normal
    self.long = . normal
  }
}
