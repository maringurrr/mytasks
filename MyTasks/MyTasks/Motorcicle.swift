//
//  Motorcicle.swift
//  MyTasks
//
//  Created by mac-001 on 6/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class Motorcicle: Vehicle {
  
  var siderCar = false
  var chain: Chain
  
  override var baseMaxSpeed: Double {
    return 150
  }
  
  override var acceleration: Double {
    var acceleration = engine.accelerateCoof + tire.accelerateCoof + brakes.accelerateCoof + accelerate.accelerateCoof + chain.accelerateCoof
    if siderCar == true {
      acceleration *= 0.9
    }
    return acceleration
  }
  
  override var maxSpeed: Double {
    var maxSpeed =  baseMaxSpeed * engine.maxSpeedCoof * tire.maxSpeedCoof * brakes.maxSpeedCoof * chain.maxSpeedCoof
    if siderCar == true {
      maxSpeed *= 0.9
    }
    return maxSpeed
  }
  
  override public  var description: String {
    return super.description + "Chain manufacture = \(chain), + siderCar = \(siderCar) "
  }
  
  init(mainColor: UIColor, engine: Engine, tire: Tire, brakes: Brakes, accelerate: Accelerate, siderCar: Bool, chain: Chain) {
    self.chain = chain
    self.siderCar = siderCar
    super.init(mainColor: mainColor, engine: engine, tire: tire, brakes: brakes, accelerate: accelerate)
    self.mainColor = mainColor
    self.tire = tire
    self.brakes = brakes
    self.accelerate = accelerate
    self.tireCount = 2
  }
  init(manufacture: Manufacture, mainColor: UIColor, siderCar: Bool) {
    self.chain = Chain(manufacture: manufacture)
    self.siderCar = siderCar
    super.init(manufacture: manufacture, mainColor: mainColor)
    self.tireCount = 2
  }
  
}
