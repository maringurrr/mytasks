//
//  Tire.swift
//  MyTasks
//
//  Created by mac-001 on 6/12/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class Tire: EnvironmentProperty {

  override var maxSpeedCoof: Double {
    var coefficient = maxSpeedAmplifier.rawValue
    switch weatherFactor {
    case .ice:
      coefficient *= 0.8
    case .rain:
      coefficient *= 0.9
    case .wind:
      coefficient *= 0.95
    case .normal:
      coefficient *= 1.0
    }
    switch temperatureFactor {
    case .cold:
      coefficient *= 0.95
    case .hot:
      coefficient *= 0.95
    case .normal:
      coefficient *= 1.0
    }
    return coefficient
  }
  
  override var accelerateCoof: Double {
    
    var coefficient = accelerateAmplifier.rawValue
    switch weatherFactor {
    case .ice:
      coefficient *= 0.6
    case .rain:
      coefficient *= 0.7
    case .wind:
      coefficient *= 0.95
    case .normal:
      coefficient *= 1.0
    }
    switch temperatureFactor {
    case .cold:
      coefficient *= 0.8
    case .hot:
      coefficient *= 1.1
    case .normal:
      coefficient *= 1.0
    }
    return coefficient
  }
  
  override init() {
    super.init()
  }
  
  override init(manufacture: Manufacture) {
    super.init()
    
    self.manufacture = manufacture
    switch manufacture {
    case .Bugatti:
      maxSpeedAmplifier = .fast
      accelerateAmplifier = .fast
    case .Lamborghini:
      maxSpeedAmplifier = .turbo
      accelerateAmplifier = .turbo
    case .BMV:
      maxSpeedAmplifier = .baseComplection
      accelerateAmplifier = .turbo
    case .Mclaren:
      maxSpeedAmplifier = .baseComplection
      accelerateAmplifier = .baseComplection
    case .Mersedes:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .fast
    case .Ferrari:
      maxSpeedAmplifier = .hiperTurbo
      accelerateAmplifier = .turbo
    case .Unknown:
      maxSpeedAmplifier = .baseComplection
      accelerateAmplifier = .slow
    }
  }
}
