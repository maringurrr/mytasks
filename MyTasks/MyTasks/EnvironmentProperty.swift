//
//  EnvironmentProperty.swift
//  MyTasks
//
//  Created by mac-001 on 6/15/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation

class EnvironmentProperty: Property {
  
  var temperatureFactor: TemperatureFactor
  var weatherFactor: WeatherFactor
  override var maxSpeedCoof: Double {
    return maxSpeedAmplifier.rawValue
  }
  
  override var accelerateCoof: Double {
    return accelerateAmplifier.rawValue
  }
  
  override init() {
    self.temperatureFactor = .normal
    self.weatherFactor = .normal
    super.init()
  }
  override init(manufacture: Manufacture) {
    self.temperatureFactor = .normal
    self.weatherFactor = .normal
    super.init()
  }
}
