//
//  Message.swift
//  MyTasks
//
//  Created by mac-001 on 7/4/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import Foundation
import ObjectMapper

class Message: Mappable {
  var id: Int?
  var userId: Int?
  var title: String?
  var body: String?
  
  
  init() {
  }
  
  required convenience init?(map: Map) {
    self.init()
  }
  
  func mapping(map: Map) {
    userId <- map["userId"]
    id <- map["id"]
    title <- map["title"]
    body <- map["body"]
  }
  
}
