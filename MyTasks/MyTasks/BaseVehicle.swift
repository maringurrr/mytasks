//
//  BaseVehicle.swift
//  MyTasks
//
//  Created by mac-001 on 6/9/17.
//  Copyright © 2017 mac-001. All rights reserved.
//

import UIKit

class BaseVehicle: CustomStringConvertible {
  
  public var description: String {
    return "MaxSpeed = \(maxSpeed), Acceleration = \(acceleration), TireCount = \(tireCount), mainColor = \(mainColor), Tire manufacture = \(tire), Brakes manufacture = \(brakes), Accelerate manufacture = \(accelerate), Tire manufacture = \(tire), "
  }
  var tire: Tire
  var mainColor: UIColor = UIColor.gray
  var baseMaxSpeed: Double {
    return 0
  }
  var accelerate: Accelerate
  var brakes: Brakes
  var tireCount: Int
  var maxSpeed: Double {
    let maxSpeed =  baseMaxSpeed * tire.maxSpeedCoof * brakes.maxSpeedCoof
    return maxSpeed
  }
  var acceleration: Double {
    let acceleration =  tire.accelerateCoof + brakes.accelerateCoof + accelerate.accelerateCoof
    return acceleration
  }
  
  init(mainColor: UIColor, tire: Tire, brakes: Brakes, accelerate: Accelerate) {
    self.mainColor = mainColor
    self.tire = tire
    self.brakes = brakes
    self.accelerate = accelerate
    self.tireCount = 4
  }
  
  init(manufacture: Manufacture, mainColor: UIColor) {
    self.mainColor = mainColor
    self.tire = Tire(manufacture: manufacture)
    self.brakes = Brakes(manufacture: manufacture)
    self.accelerate = Accelerate(manufacture: manufacture)
    self.tireCount = 4
  }
  
  func race(track: Track) -> String {
    var time: Double = 0
    for segment in track.segments {
      time += timeSegment(segment: segment)
    }
    return "\(Int(time)) sec"
  }
  
  private func timeSegment(segment: Segment) -> Double {
    
    tire.weatherFactor = segment.weather
    tire.temperatureFactor = segment.temperature
    brakes.weatherFactor = segment.weather
    brakes.temperatureFactor = segment.temperature
    
    let timeForAccelerate = maxSpeed / acceleration
    let s = acceleration * timeForAccelerate * timeForAccelerate / 2
    
    if Double(segment.long.rawValue) > s {
      let dist = Double(segment.long.rawValue) - s
      let time = 2 * dist / maxSpeed
      return time + timeForAccelerate
    } else {
      return sqrt(acceleration * s / 2)
    }
  }
}
